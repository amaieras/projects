﻿using DiagnozaAuto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiagnozaAuto.Controllers
{
    public class CommentsController : Controller
    {
        // GET: Comments
        public ActionResult Comments()
        {
            return View();
        }

        //[HttpPost]
        //public ActionResult Comment(CommentViewModel viewModel)
        //    {
        //    if (ModelState.IsValid)
        //        {
        //        //Mapping code - alternatively try AutoMapper
        //        var dataComment = new Comment();
        //        dataComment.PostId = viewModel.Id;
        //        dataComment.Author = viewModel.Author;
        //        dataComment.Body = viewModel.Body;
        //        dataComment.Email = viewModel.Email;

        //        // Create comment and save changes
        //        //commentRepository.Create(comment);
        //        //commentRepository.SaveChanges();

        //        return new EmptyResult();
        //        }

        //    var modelErrors = this.BuildModelErrors();
        //    // return a bad request to signify that adding the comment failed
        //    HttpContext.Response.StatusCode = 400;
        //    // return errors as Json, read by javascript
        //    return Json(modelErrors);
        //    }
        //[HttpPost]
        //public ActionResult Comment()
        //    {
        //    return View();
        //    }

        /// <summary>
        /// Build a list of model errors from model state.  
        /// This method flattens the model state errors.
        /// </summary>
        /// <returns>A list of Keys and Error messages</returns>
        private List<ModelError> BuildModelErrors()
            {
            var modelErrors = new List<ModelError>();
            var erroneousFields = this.ModelState.Where(ms => ms.Value.Errors.Any())
                                                 .Select(x => new { x.Key, x.Value.Errors });

            foreach (var erroneousField in erroneousFields)
                {
                var fieldKey = erroneousField.Key;
                var fieldErrors = erroneousField.Errors.Select(error =>
                                                    new ModelError(fieldKey, error.ErrorMessage));
                modelErrors.AddRange(fieldErrors);
                }
            return modelErrors;
            }

        //Class to hold model errors and the corresponding field key
        private class ModelError
            {
            public ModelError(string key, string errorMessage)
                {
                Key = key;
                ErrorMessage = errorMessage;
                }

            public string Key { get; set; }
            public string ErrorMessage { get; set; }
            }       
    }
}