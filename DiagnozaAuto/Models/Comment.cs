﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DiagnozaAuto.Models
    {
    public class Comment
        {

        [Required]
        public int Id { get; set; }

        [StringLength(50)]
        public string Author { get; set; }

        [Required]
        public string Body { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        public int PostId { get; set; }

        // Link to existing Post class 
        public virtual Post Post { get; set; }
        }

    public class Post
        {
        // Current Properties...

        // New relationship property
        public virtual ICollection<Comment> Comments { get; set; }
        }
    }